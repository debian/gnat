Source: gnat
Maintainer: Ludovic Brenta <lbrenta@debian.org>
Uploaders: Nicolas Boulenguez <nicolas@debian.org>
Section: devel
Priority: optional
Build-Depends: debhelper-compat (= 13)
Rules-Requires-Root: no
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/debian/gnat
Vcs-Git: https://salsa.debian.org/debian/gnat.git

Package: gnat
Architecture: all
Depends: ${misc:Depends},
# This line is parsed by debian/gnat.links:
 gnat-14,
Suggests: ada-reference-manual-2012
# The unversioned symbolic links to gnat tools have moved from gnat-X.
Breaks: gnat-7, gnat-8, gnat-9, gnat-10, gnat-11, gnat-12, gnat-13,
Replaces: gnat-7, gnat-8, gnat-9, gnat-10, gnat-11, gnat-12, gnat-13,
Description: GNU Ada compiler
 GNAT is a full-featured Ada 2012 compiler.  A quote from
 http://www.adaic.org says: "Easily reused and maintained, readable
 and user friendly, Ada code facilitates such massive software
 projects as the Space Station and the Paris Metro. It has proven to
 be extraordinarily robust in decades' worth of daily field tests
 under the most rigorous conditions in which millions of lives have
 been at stake."  Ada is the language for real-world, mission-critical
 programming.
 .
 At the same time, Ada's radical type safety helps novice programmers
 avoid many common mistakes and deliver their software on time (see
 http://www.adaic.org/atwork/trains.html).
 .
 This empty package depends on the default version of the Ada compiler
 for Debian, which is part of the GNU Compiler Collection. Its
 enforces the same version for all Ada compilations, as described in
 the Debian Ada Policy.
